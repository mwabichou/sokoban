SRC	=	src/main.c \
		src/affichage.c \
		src/cibles.c    \
		src/deplacement.c \
		src/grille.c \
		src/jeu.c \
		src/joueur.c \
		src/map.c \
		src/menu.c \
		src/niveau.c \
		src/regles.c \
		src/sauvegarde.c \
		src/score.c     \
		src/chrono.c

OBJ	=	$(SRC:.c=.o)

NAME	=	sokoban

all:	$(NAME)

$(NAME):
	gcc $(SRC) -Wall -v -I src/include -o $(NAME) -g -lm

clean:
	rm -f $(OBJ)
	rm -rf *.dSYM

fclean:	clean
	rm -f $(NAME)

re:	fclean all
run:
	gcc $(SRC) -Wall -v -I src/include -o $(NAME) -g -lm
	./sokoban

.PHONY:	all clean fclean re