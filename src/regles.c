#include "sokoban.h"
void afficheRegles()
{
    FILE *fr = fopen("./regles/regles_du_jeu.txt", "r+");
    if (!fr)
    {
        perror("erreur d'ouverture du fichier");
        exit(-1);
    }
    char *buffer = malloc(sizeof(char) * SIZE_BUFF);
    while (fgets(buffer, SIZE_BUFF, fr) != 0)
    {
        printf("\t\t\t%s", buffer);
    }
    free(buffer);
    fclose(fr);
}