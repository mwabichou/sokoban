#include "sokoban.h"
map *initMap(int niveau)
{
    map *Map = malloc(sizeof(map));
    sprintf(Map->nomFichierMap, "./maps/map%d.txt", niveau);
    Map->longueur = LongueurGrille(Map->nomFichierMap);
    Map->largeur = LargeurGrille(Map->nomFichierMap);
    GrilleFromFichier(Map);
    nombreCibles(Map);
    Map->nbCaisses = Map->nbCibles;
    positionCibles(Map);
    Map->score = 0;
    nombreCaissesSurCibles(Map);
    return Map;
}