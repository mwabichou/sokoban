#include "sokoban.h"
void sauvegrade(int niveau)
{
    FILE *f1 = fopen("./sauvegarde/sauvegarde.txt", "w+");
    if (!f1)
    {
        perror("erreur d'ouverture du fichier ");
        exit(-1);
    }
    fprintf(f1, "%d", niveau);
    fclose(f1);
}
int reprendre()
{
    int niveau;
    int nbLu;
    FILE *f1 = fopen("./sauvegarde/sauvegarde.txt", "r+");
    if (!f1)
    {
        perror("erreur d'ouverture du fichier ");
        exit(-1);
    }
    nbLu = fscanf(f1, "%d", &niveau);
    fclose(f1);
    (nbLu == 0) ? niveau = 1 : niveau;
    return niveau;
}
