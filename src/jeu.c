#include "sokoban.h"
void loopJeu(int niveauDebut)
{
    int niveau = niveauDebut;
    int gagne = 0;
    while (niveau <= NIVEAUMAX)
    {
        gagne = loopNiveau(niveau);
        if (gagne)
        {
            niveau++;
            sauvegrade(niveau);
        }
        else
            break;
    }
}
