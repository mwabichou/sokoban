#include "sokoban.h"
int score(map *Map, joueur *P)
{
    printf("\n\n\n\n\t\t\t\t   score : \033[1;31m%dxp\033[0m", Map->nbCibles * 100 - Map->nbCaissesSurCibles * 10 - P->nbMoves);
    return Map->nbCibles * 100 - Map->nbCaissesSurCibles * 10 - P->nbMoves;
}
int victoire(map *Map)
{
    if (Map->nbCaissesSurCibles == Map->nbCibles)
    {
        return 1;
    }
    return 0;
}