#include "sokoban.h"

char lectureTouche()
{
    char ch;
    system("stty -echo");
    system("stty cbreak");
    ch = getchar();
    system("stty echo");
    system("stty -cbreak");
    return ch;
}

void deplacement(char c, map *Map, joueur *P)
{
    switch (c)
    {
    case 'h':
    case 'z':
        if (Map->grille[P->y - 1][P->x] == ' ' || Map->grille[P->y - 1][P->x] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y - 1][P->x] = 'P';
            P->y -= 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y - 1][P->x] == 'C' && Map->grille[P->y - 2][P->x] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y - 1][P->x] = 'P';
            Map->grille[P->y - 2][P->x] = 'C';
            P->y -= 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y - 1][P->x] == 'C')
        {
            if (Map->grille[P->y - 2][P->x] == ' ')
            {
                Map->grille[P->y][P->x] = ' ';
                Map->grille[P->y - 1][P->x] = 'P';
                Map->grille[P->y - 2][P->x] = 'C';
                P->y -= 1;
                P->nbMoves += 1;
            }
        }
        break;
    case 'b':
    case 's':
        if (Map->grille[P->y + 1][P->x] == ' ' || Map->grille[P->y + 1][P->x] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y + 1][P->x] = 'P';
            P->y += 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y + 1][P->x] == 'C' && Map->grille[P->y + 2][P->x] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y + 1][P->x] = 'P';
            Map->grille[P->y + 2][P->x] = 'C';
            P->y += 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y + 1][P->x] == 'C')
        {
            if (Map->grille[P->y + 2][P->x] == ' ')
            {
                Map->grille[P->y][P->x] = ' ';
                Map->grille[P->y + 1][P->x] = 'P';
                Map->grille[P->y + 2][P->x] = 'C';
                P->y += 1;
                P->nbMoves += 1;
            }
        }
        break;
    case 'g':
    case 'q':
        if (Map->grille[P->y][P->x - 1] == ' ' || Map->grille[P->y][P->x - 1] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y][P->x - 1] = 'P';
            P->x -= 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y][P->x - 1] == 'C' && Map->grille[P->y][P->x - 2] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y][P->x - 1] = 'P';
            Map->grille[P->y][P->x - 2] = 'C';
            P->x -= 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y][P->x - 1] == 'C')
        {
            if (Map->grille[P->y][P->x - 2] == ' ')
            {
                Map->grille[P->y][P->x] = ' ';
                Map->grille[P->y][P->x - 1] = 'P';
                Map->grille[P->y][P->x - 2] = 'C';
                P->x -= 1;
                P->nbMoves += 1;
            }
        }
        break;
    case 'd':
        if (Map->grille[P->y][P->x + 1] == ' ' || Map->grille[P->y][P->x + 1] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y][P->x + 1] = 'P';
            P->x += 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y][P->x + 1] == 'C' && Map->grille[P->y][P->x + 2] == 'I')
        {
            Map->grille[P->y][P->x] = ' ';
            Map->grille[P->y][P->x + 1] = 'P';
            Map->grille[P->y][P->x + 2] = 'C';
            P->x += 1;
            P->nbMoves += 1;
        }
        else if (Map->grille[P->y][P->x + 1] == 'C')
        {
            if (Map->grille[P->y][P->x + 2] == ' ')
            {
                Map->grille[P->y][P->x] = ' ';
                Map->grille[P->y][P->x + 1] = 'P';
                Map->grille[P->y][P->x + 2] = 'C';
                P->x += 1;
                P->nbMoves += 1;
            }
        }
        break;
    default:
        break;
    }
    remiseCibles(Map);
}
