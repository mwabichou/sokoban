#include "sokoban.h"
void nombreCaissesSurCibles(map *Map)
{
    Map->nbCaissesSurCibles = 0;
    for (int i = 0; i < Map->nbCibles; i++)
    {
        if (Map->grille[Map->tabCibles[i].y][Map->tabCibles[i].x] == 'C')
        {
            Map->nbCaissesSurCibles += 1;
        }
    }
}

void nombreCibles(map *Map)
{
    Map->nbCibles = 0;
    for (int i = 0; i < Map->longueur; i++)
    {
        for (int j = 0; j < Map->largeur; j++)
        {
            if (Map->grille[i][j] == 'I')
            {
                Map->nbCibles++;
            }
        }
    }
}
void positionCibles(map *Map)
{
    Map->tabCibles = malloc(sizeof(cible) * Map->nbCibles);
    int k = 0;
    for (int i = 0; i < Map->longueur; i++)
    {
        for (int j = 0; j < Map->largeur; j++)
        {
            if (Map->grille[i][j] == 'I')
            {
                Map->tabCibles[k].y = i;
                Map->tabCibles[k].x = j;
                k++;
            }
        }
    }
}

void remiseCibles(map *Map)
{
    for (int i = 0; i < Map->nbCibles; i++)
    {
        if (Map->grille[Map->tabCibles[i].y][Map->tabCibles[i].x] == ' ')
        {
            Map->grille[Map->tabCibles[i].y][Map->tabCibles[i].x] = 'I';
        }
    }
}