#include "sokoban.h"
joueur *initJoueur(map *Map)
{
    joueur *P = malloc(sizeof(joueur));
    positionInitialeJoueur(Map, P);
    P->nbMoves = 0;
    return P;
}
void positionInitialeJoueur(map *Map, joueur *P)
{
    for (int i = 0; i < Map->longueur; i++)
    {
        for (int j = 0; j < Map->largeur; j++)
        {
            if (Map->grille[i][j] == 'P')
            {
                P->x = j;
                P->y = i;
            }
        }
    }
}