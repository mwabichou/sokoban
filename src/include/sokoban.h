#ifndef sokoban
#define sokoban
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#define SIZE_BUFF 512
#define NIVEAUMAX 21
typedef struct joueur
{
    int x;
    int y;
    int nbMoves;
} joueur;

typedef struct cible
{
    int x;
    int y;
} cible;
typedef struct map
{
    char nomFichierMap[20];
    char **grille;
    int longueur;
    int largeur;
    int nbCibles;
    int nbCaisses;
    int nbCaissesSurCibles;
    int score;
    cible *tabCibles;
} map;
char **mallocGrille(int longueur, int larg);
void freeGrille(map *Map);
int LargeurGrille(char *nomFichierMap);
int LongueurGrille(char *nomFichierMap);
void afficheGrille(map *Map);
void GrilleFromFichier(map *Map);
void positionInitialeJoueur(map *Map, joueur *P);
void nombreCaissesSurCibles(map *Map);
void nombreCibles(map *Map);
void positionCibles(map *Map);
int victoire(map *Map);
int score(map *Map, joueur *P);
void remiseCibles(map *Map);
void deplacement(char c, map *Map, joueur *P);
char lectureTouche();
map *initMap(int niveau);
joueur *initJoueur(map *Map);
void sauvegrade(int niveau);
int reprendre();
void loopJeu(int niveauDebut);
int loopNiveau(int niveau);
void afficheRegles();
void mainMenu();
void ecranVictoire(int niveau, int score, double temps);
int main(int argc, char **argv);
int meilleurScore(int score, int niveau);
#endif /*sokoban*/