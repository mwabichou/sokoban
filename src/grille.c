#include "sokoban.h"
char **mallocGrille(int longueur, int larg)
{
    char **grille = malloc(sizeof(char *) * longueur);
    if (!grille)
    {
        exit(-1);
        return NULL;
    }
    for (int i = 0; i < longueur; i++)
    {
        grille[i] = malloc(sizeof(char) * larg);
        if (!grille[i])
        {
            exit(-1);
            return NULL;
        }
    }
    return grille;
}

void freeGrille(map *Map)
{
    for (int i = 0; i < Map->longueur; i++)
    {
        free(Map->grille[i]);
    }
    free(Map->grille);
}

int LargeurGrille(char *nomFichierMap)
{
    FILE *f = fopen(nomFichierMap, "r+");
    if (!f)
    {
        perror("Erreur d'ouverture du fichier");
        exit(-1);
    }
    char *buffer = malloc(sizeof(char) * SIZE_BUFF);
    int nbCaracteresMax = 0;
    while ((fgets(buffer, SIZE_BUFF, f)) != 0)
    {
        if (strlen(buffer) >= nbCaracteresMax)
        {
            nbCaracteresMax = strlen(buffer);
        }
    }
    fclose(f);
    free(buffer);
    return nbCaracteresMax;
}
int LongueurGrille(char *nomFichierMap)
{
    FILE *f = fopen(nomFichierMap, "r+");
    if (!f)
    {
        perror("Erreur d'ouverture du fichier");
        exit(-1);
    }
    char element;
    int longueur = 1;
    while ((element = fgetc(f)) != EOF)
    {
        if (element == '\n')
        {
            longueur += 1;
        }
    }
    fclose(f);
    return longueur;
}
void GrilleFromFichier(map *Map)
{
    Map->grille = mallocGrille(Map->longueur, Map->largeur);
    FILE *f = fopen(Map->nomFichierMap, "r+");
    if (!f)
    {
        perror("Erreur d'ouverture du fichier");
        exit(-1);
    }
    for (int i = 0; i < Map->longueur; i++)
    {
        fgets(Map->grille[i], SIZE_BUFF, f);
    }
    fclose(f);
}