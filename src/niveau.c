#include "sokoban.h"

void ecranVictoire(int niveau, int score, double temps)
{

    printf("\n\n\t\t\t\tBravo, vous avez réussi ce niveau (%d) de Sokoban apres  \033[1;31m%.2f\033[0m secondes ! \n", niveau, temps);
    printf("\t\t\t\tAppuyez sur une touche pour continuer\n");
    char ch = lectureTouche();
}
int loopNiveau(int niveau)
{
    map *Map = initMap(niveau);
    joueur *P = initJoueur(Map);
    time_t debut = time(NULL);
    int ch;
    int run = 1;
    int gagne = 0;
    double temps = 0;
    while (run && (ch != 'l'))
    {

        score(Map, P);
        printf(" || Niveau : \033[1;31m%d\033[0m || Caisses sur cibles : \033[1;31m%d\033[0m/%d ", niveau, Map->nbCaissesSurCibles, Map->nbCibles);
        nombreCaissesSurCibles(Map);
        afficheGrille(Map);
        printf("\t\t\t\tappuyer sur \033[1;31mR\033[0m pour recommencer le niveau ou \033[1;31mL\033[0m pour quitter\n");
        if (victoire(Map))
        {
            gagne = 1;
            run = 0;
            time_t fin = time(NULL);
            temps = difftime(fin, debut);
            ecranVictoire(niveau, score, temps);
        }
        ch = lectureTouche();
        deplacement(ch, Map, P);
        system("clear");
        if (ch == 'r')
        {
            gagne = loopNiveau(niveau);
            run = 0;
        }
    }
    freeGrille(Map);
    free(P);
    system("clear");
    return gagne;
}