#include "sokoban.h"
void afficheGrille(map *Map)
{
    printf("\n\n\n\n");
    for (int i = 0; i < Map->longueur; i++)
    {
        for (int j = 0; j < Map->largeur; j++)
        {
            if (j == 0)
            {
                if (Map->largeur > 60)
                    printf("\t\t\t\t");
                else
                    printf("\t\t\t\t\t\t");
            }
            if (Map->grille[i][j] == 'P')
            {
                printf("\033[1;31m%c\033[0m", Map->grille[i][j]);
            }
            else if (Map->grille[i][j] == 'C')
            {
                printf("\033[1;33m%c\033[0m", Map->grille[i][j]);
            }
            else if (Map->grille[i][j] == 'I')
            {
                printf("\033[1;32m%c\033[0m", Map->grille[i][j]);
            }
            else
            {
                printf("%c", Map->grille[i][j]);
            }
        }
    }
    printf("\n\n\n\n");
}