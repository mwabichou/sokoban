#include "sokoban.h"
void mainMenu()
{
    int run = 1;
    while (run)
    {
        printf("\033[34m\n\t\t\t______________________________SOKOBAN___________________________________\033[0m");
        printf("\n\t\t\t|\t\t\t1. Jouer\t\t\t\t\t|\n");
        printf("\t\t\t|\t\t\t2. Reprendre\t\t\t\t\t|\n");
        printf("\t\t\t|\t\t\t3. Regles du jeu\t\t\t\t|\n");
        printf("\t\t\t|\t\t\t4. Choisir Niveau\t\t\t\t|\n");
        printf("\t\t\t|\t\t\t5. Quitter le jeu\t\t\t\t|\n");
        printf("\033[34m\t\t\t____________________________________________________________\033[1;31mBY M.WASSIM.A%c\033[0m\n\033[0m");
        int choice;
        int niveau;
        printf("Choix -> ");
        scanf("%d", &choice);
        fflush(stdin);
        switch (choice)
        {
        case 1:
            system("clear");
            loopJeu(1);
            break;
        case 2:
            system("clear");
            niveau = reprendre();
            loopJeu(niveau);
            break;
        case 3:
            system("clear");
            afficheRegles();
            break;
        case 4:
            fflush(stdin);
            printf("Niveau (21 max) -> ");
            scanf("%d", &niveau);
            loopJeu(niveau);
            break;
        case 5:
            printf("Aurevoir ! :) \n");
            run = 0;
            break;
        default:
            printf("Choix invalide\n");
            break;
        }
    }
}