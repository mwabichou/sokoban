![Screenshot](img/capture.png)

Dans le cadre de notre deuxiemme année de licence en Informatique à l'université Clermont Auvergne. Il nous est proposé de réaliser un projet intitulé 《SOKOBAN》 permettant de mettre en pratique nos connaissances et nos compétences professionnelles en C et ses différentes bibliothèques.

## Pour commencer

Placez vous dans le dossier du projet avec la commande cd

### Makefile

Le makefile est un fichier qui permet de compiler et d'executer le projet en une seule commande.

### Installation

1 - Lancer un terminal dans le dossier du projet
Executez la commande `make run`, cette derniere lancera le jeu et il vous restera qu'a l’essayer.

## Fabriqué avec

- [C](https://devdocs.io/c/) - C est un langage de programmation impératif généraliste, de bas niveau.

## Versions

**Dernière version stable :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.isima.fr/mwabichou/sokoban.git)

## Auteurs

- **Mohamed Wassim Abichou** _alias_ [@mwabichou](https://gitlab.isima.fr/mwabichou)

Lisez la liste des [contributeurs](https://gitlab.isima.fr/mwabichou/sokoban/-/project_members) pour voir qui à aidé au projet !

## License

Ce projet est sous la licence `AMW`
